/*
  Installing/using this software, you agree that this software is
  only for study purposes and its authors and service providers  
  take no responsibilities for any consequences.
*/
function _check_regex_list(i,o) 
{
	":80/"===o.slice(0,4)&&(o=o.slice(3));
	
	for(var c=0;c<i.length;c++)
		if(i[c].test(o)) return!0;return!1
}

function _check_patterns(i,o,c,m) {
	return!(!i.hasOwnProperty(o)||!_check_regex_list(i[o],c.slice(m+o.length)))||!!_check_regex_list(i.any,c.slice(m))
}

function _find_proxy(i,o,c,m){
	return _check_patterns(i.white,o,c,m)? "DIRECT" : _check_patterns(i.proxy,o,c,m)? _proxy_str : "DIRECT"
}
	
function FindProxyForURL(i,o){
	var c=i.slice(0,6);
	
	return"http:/"===c?_find_proxy(_http_map,o,i,7):"https:"===c?_find_proxy(_https_map,o,i,8):"DIRECT"
}

var _http_map= 
{
	white: 
	{
		any:[],
		"bangumi.bilibili.com":[/^\/index\/ding\-count\.json$/i]
	},
	proxy: {
		any: [],
		"v.youku.com":[/^\//i,/^\//i,/^\//i],
		"api.youku.com":[/^\//i],
		"play.youku.com":[/^\//i],
		"play-dxk.youku.com":[/^\//i],
		"play-ali.youku.com":[/^\//i],
		"list.youku.com":[/^\//i,/^\//i,/^\//i],
		"ups.youku.com":[/^\//i],
		"www.tudou.com":[/^\//i,/^\//i,/^\//i,/^\//i,/^\//i,/^\//i,/^\//i],
		"s.plcloud.music.qq.com":[/^\//i],
		"i.y.qq.com":[/^\//i,/^\//i],
		"c.y.qq.com":[/^\//i,/^\//i],
		"api.unipay.qq.com":[/^\//i],
		"hot.vrs.sohu.com":[/^\//i],
		"live.tv.sohu.com":[/^\//i],
		"pad.tv.sohu.com":[/^\//i],
		"my.tv.sohu.com":[/^\//i],
		"hot.vrs.letv.com":[/^\//i],
		"api.le.com":[/^\//i],
		"player.pc.le.com":[/^\//i],
		"player-pc.le.com":[/^\//i],
		"data.video.qiyi.com":[/^\//i,/^\//i,/^\//i],
		"data.video.iqiyi.com":[/^\//i,/^\//i,/^\//i],
		"cache.vip.qiyi.com":[/^\//i],
		"cache.video.qiyi.com":[/^\//i],
		"cache.vip.iqiyi.com":[/^\//i],
		"cache.video.iqiyi.com":[/^\//i],
		"iplocation.geo.qiyi.com":[/^\//i,/^\//i],
		"iplocation.geo.iqiyi.com":[/^\//i],
		"v.api.hunantv.com":[/^\//i,/^\//i],
		"mobile.api.hunantv.com":[/^\//i,/^\//i],
		"v.api.mgtv.com":[/^\//i],
		"pcweb.api.mgtv.com":[/^\//i],
		"acc.music.qq.com":[/^\//i],
		"api.appsdk.soku.com":[/^\//i],
		"app.bilibili.com":[/^\//i,/^\//i],
		"bangumi.bilibili.com":[/^\//i,/^\//i,/^\//i],
		"info.zb.qq.com":[/^\//i],
		"info.zb.video.qq.com":[/^\//i],
		"qzs.qq.com":[/^\//i],
		"dispatcher.video.sina.com.cn":[/^\//i],
		"geo.js.kankan.com":[/^\//i],
		"web-play.pptv.com":[/^\//i],
		"web-play.pplive.cn":[/^\//i],
		"tools.aplusapi.pptv.com":[/^\//i],
		"live.pptv.com":[/^\//i],
		"dyn.ugc.pps.tv":[/^\//i],
		"v.pps.tv":[/^\//i],
		"inner.kandian.com":[/^\//i],
		"zb.s.qq.com":[/^\//i],
		"ip.kankan.com":[/^\//i],
		"music.sina.com.cn":[/^\//i,/^\//i],
		"play.baidu.com":[/^\//i],
		"v.iask.com":[/^\//i,/^\//i],
		"tv.weibo.com":[/^\//i],
		"wtv.v.iask.com":[/^\//i,/^\//i,/^\//i],
		"video.sina.com.cn":[/^\//i],
		"www.yinyuetai.com":[/^\//i,/^\//i],
		"www.kugou.com":[/^\//i],
		"www.kuwo.cn":[/^\//i],
		"antiserver.kuwo.cn":[/^\//i],
		"ipcheck.kuwo.cn":[/^\//i],
		"api.letv.com":[/^\//i,/^\//i,/^\//i,/^\//i,/^\//i,/^\//i],
		"api.www.letv.com":[/^\//i],
		"st.live.letv.com":[/^\//i],
		"live.gslb.letv.com":[/^\//i,/^\//i],
		"live.g3proxy.lecloud.com":[/^\//i],
		"api.live.letv.com":[/^\//i],
		"static.itv.letv.com":[/^\//i],
		"ip.apps.cntv.cn":[/^\//i],
		"vdn.apps.cntv.cn":[/^\//i,/^\//i,/^\//i,/^\//i,/^\//i],
		"vdn.live.cntv.cn":[/^\//i,/^\//i],
		"cctv1.vtime.cntv.cloudcdn.net":[/^\//i],
		"cctv5.vtime.cntv.cloudcdn.net":[/^\//i],
		"cctv5plus.vtime.cntv.cloudcdn.net":[/^\//i],
		"cctv13.vtime.cntv.cloudcdn.net":[/^\//i],
		"sports1pull.live.wscdns.com":[/^\//i],
		"vip.sports.cntv.cn":[/^\//i,/^\//i,/^\//i],
		"www.youku.com":[/^\//i],
		"www.soku.com":[/^\//i],
		"search.api.3g.youku.com":[/^\//i],
		"search.api.3g.tudou.com":[/^\//i],
		"api.tv.sohu.com":[/^\//i,/^\//i],
		"ac.qq.com":[/^\//i,/^\//i,/^\//i],
		"live.api.hunantv.com":[/^\//i],
		"www.qie.tv":[/^\//i],
		"www.bilibili.com":[/^\//i,/^\//i],
		"interface.bilibili.com":[/^\//i,/^\//i],
		"m10.music.126.net":[/^\//i],
		"www.xiami.com":[/^\//i,/^\//i,/^\//i],
		"lixian.xunlei.com":[/^\//i],
		"lixian.vip.xunlei.com":[/^\//i],
		"dynamic.cloud.vip.xunlei.com":[/^\//i],
		"cloud.vip.xunlei.com":[/^\//i],
		"www.iqiyi.com":[/^\//i],
		"ark.letv.com":[/^\//i],
		"search.lekan.letv.com":[/^\//i],
		"pay.youku.com":[/^\//i],
		"pay.tudou.com":[/^\//i],
		"aid.video.qq.com":[/^\//i],
		"aidbak.video.qq.com":[/^\//i],
		"pay.video.qq.com":[/^\//i],
		"paybak.video.qq.com":[/^\//i],
		"ssports.com":[/^\//i],
		"ssports.smgbb.cn":[/^\//i],
		"a.play.api.3g.youku.com":[/^\//i],
		"i.play.api.3g.youku.com":[/^\//i,/^\//i],
		"api.3g.youku.com":[/^\//i,/^\//i,/^\//i,/^\//i,/^\//i],
		"tv.api.3g.youku.com":[/^\//i,/^\//i,/^\//i],
		"play.api.3g.youku.com":[/^\//i,/^\//i,/^\//i],
		"i-play.mobile.youku.com":[/^\//i],
		"play.api.3g.tudou.com":[/^\//i],
		"tv.api.3g.tudou.com":[/^\//i],
		"api.3g.tudou.com":[/^\//i],
		"access.tv.sohu.com":[/^\//i],
		"iface.iqiyi.com":[/^\//i,/^\//i],
		"iface2.iqiyi.com":[/^\//i,/^\//i,/^\//i],
		"cache.m.iqiyi.com":[/^\//i],
		"dynamic.app.m.letv.com":[/^\//i],
		"dynamic.meizi.app.m.letv.com":[/^\//i],
		"dynamic.search.app.m.letv.com":[/^\//i],
		"dynamic.live.app.m.letv.com":[/^\//i],
		"listso.m.areainfo.ppstream.com":[/^\//i],
		"epg.api.pptv.com":[/^\//i],
		"play.api.pptv.com":[/^\//i],
		"m.letv.com":[/^\//i],
		"api.mob.app.letv.com":[/^\//i],
		"static.api.sports.letv.com":[/^\//i],
		"api.itv.letv.com":[/^\//i],
		"data.bilibili.com":[/^\//i],
		"3g.music.qq.com":[/^\//i],
		"mqqplayer.3g.qq.com":[/^\//i],
		"proxy.music.qq.com":[/^\//i],
		"proxymc.qq.com":[/^\//i],
		"ip2.kugou.com":[/^\//i],
		"ip.kugou.com":[/^\//i],
		"client.api.ttpod.com":[/^\//i],
		"mobi.kuwo.cn":[/^\//i],
		"nmobi.kuwo.cn":[/^\//i],
		"mobilefeedback.kugou.com":[/^\//i],
		"tingapi.ting.baidu.com":[/^\//i],
		"music.baidu.com":[/^\//i],
		"serviceinfo.sdk.duomi.com":[/^\//i],
		"spark.api.xiami.com":[/^\//i,/^\//i,/^\//i,/^\//i],
	}
},
		
_https_map=
{
	white:
	{
		any:[]
	},
	
	proxy:
	{
		any:[],
		"ups.youku.com":[/^\//i],
		"www.bilibili.com":[/^\//i],
		"interface.bilibili.com":[/^\//i],
		"bangumi.bilibili.com":[/^\//i],
		"app.bilibili.com":[/^\//i],
		"openapi.youku.com":[/^\//i],
	}
},
		
_proxy_str="PROXY proxy.uku.im:443; DIRECT;";